
deque<Bit> interleave_bitstream(deque<Bit> in_bitstream, deque<Bit> int_bits, vector<int> interleaver, int n_bits);
deque<int> interleave_intstream(deque<int> in_bitstream, deque<int> int_bits, vector<int> interleaver, int n_bits);
vector<float> interleave_floatstream(vector<float> in_bitstream, vector<int> interleaver, int n_bits);
vector<float> deinterleave_floatstream(vector<float> in_bitstream, vector<int> interleaver, int n_bits);
deque<Bit> deinterleave_bitstream(deque<Bit> in_bitstream, deque<Bit> int_bits, vector<int> interleaver, int n_bits);
vector<int> read_interleaver(char* filename, int* interleaver_size);

vector<int> read_interleaver(char* filename, int* interleaver_size) {

	ifstream interleaver_stream; 
	interleaver_stream.open(filename);

	vector<int> interleaver;
	int temp_num; 

	interleaver_stream >> *interleaver_size; 

	cout << "number of interleaver bits is: " << *interleaver_size << '\n'; 

	for (int i=0; i<*interleaver_size; i++) {
		interleaver_stream >> temp_num;	
		interleaver.push_back(temp_num);
	}

	interleaver_stream.close();

	return interleaver; 
}



deque<Bit> interleave_bitstream(deque<Bit> in_bitstream, deque<Bit> int_bits, vector<int> interleaver, int n_bits) {
	//create a temp bitstream to hold the interleaved bits
	deque<Bit> temp_bitstream(n_bits,0);

	int i = 0; 
	int n; 
	for (deque<Bit>::iterator it = in_bitstream.begin(); it != in_bitstream.begin()+n_bits; ++it) {
		n = interleaver[i];
		temp_bitstream[n] = *it; 
		i++;
	}

	//push back the bits into the interleaved bitstream 
	for (deque<Bit>::iterator it = temp_bitstream.begin(); it != temp_bitstream.end(); ++it) {
		int_bits.push_back(*it);
	}
	return int_bits; 
}

deque<int> interleave_intstream(deque<int> in_bitstream, deque<int> int_bits, vector<int> interleaver, int n_bits) {
	//create a temp bitstream to hold the interleaved bits
	deque<int> temp_bitstream(n_bits,0);

	int i = 0; 
	int n; 
	for (deque<int>::iterator it = in_bitstream.begin(); it != in_bitstream.begin()+n_bits; ++it) {
		n = interleaver[i];
		temp_bitstream[n] = *it; 
		i++;
	}

	//push back the bits into the interleaved bitstream 
	for (deque<int>::iterator it = temp_bitstream.begin(); it != temp_bitstream.end(); ++it) {
		int_bits.push_back(*it);
	}
	return int_bits; 
}

vector<float> interleave_floatstream(vector<float> in_bitstream, vector<int> interleaver, int n_bits) {
	//create a temp bitstream to hold the interleaved bits
	vector<float> temp_bitstream(n_bits,0);
	vector<float> int_bits; 

	int i = 0; 
	int n; 
	for (vector<float>::iterator it = in_bitstream.begin(); it != in_bitstream.begin()+n_bits; ++it) {
		n = interleaver[i];
		temp_bitstream[n] = *it; 
		i++;
	}

	//push back the bits into the interleaved bitstream 
	for (vector<float>::iterator it = temp_bitstream.begin(); it != temp_bitstream.end(); ++it) {
		int_bits.push_back(*it);
	}
	return int_bits; 
}

deque<Bit> deinterleave_bitstream(deque<Bit> in_bitstream, deque<Bit> int_bits, vector<int> interleaver, int n_bits) {
	//create a temp bitstream to hold the deinterleaved bits
	deque<Bit> temp_bitstream(n_bits,0);

	//cout << "DEINTERLEAVING! \n"; 
	for (int i = 0; i< interleaver.size(); i++) {
		temp_bitstream[i] = in_bitstream[ interleaver.at(i) ]; 
		//cout << in_bitstream[ interleaver.at(i) ].getSoft() << '\n';
	}

	//push back the bits into the deinterleaved bitstream 
	for (deque<Bit>::iterator it = temp_bitstream.begin(); it != temp_bitstream.end(); ++it) {
		int_bits.push_back(*it);
	}
	return int_bits; 
}

vector<float> deinterleave_floatstream(vector<float> in_bitstream, vector<int> interleaver, int n_bits) {
	//create a temp bitstream to hold the deinterleaved bits
	vector<float> temp_bitstream(n_bits,0);
	vector<float> int_bits;

	//cout << "DEINTERLEAVING! \n"; 
	for (int i = 0; i< interleaver.size(); i++) {
		temp_bitstream[i] = in_bitstream[ interleaver.at(i) ]; 
		//cout << in_bitstream[ interleaver.at(i) ].getSoft() << '\n';
	}

	//push back the bits into the deinterleaved bitstream 
	for (vector<float> ::iterator it = temp_bitstream.begin(); it != temp_bitstream.end(); ++it) {
		int_bits.push_back(*it);
	}
	return int_bits; 
}