

cparse: constellation_parser.cpp Point.cpp 
	g++-4.8 -std=c++11 -mmacosx-version-min=10.6 -o cparse constellation_parser.cpp Point.cpp 

sra: sra.cpp accumulate.cpp 
	g++-4.8 -std=c++11 -mmacosx-version-min=10.6 -o sra sra.cpp accumulate.cpp 

testsra: testSRA.cpp accumulate.cpp 
	g++-4.8 -std=c++11 -mmacosx-version-min=10.6 -o testsra testsra.cpp accumulate.cpp 

clean:
	rm *.o