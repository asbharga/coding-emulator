/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// To use this file type cparse CONSTELLATION INTERLEAVER START_EBNO d_EBNO END_EBNO 
// ./cparse QAM16.txt PGI_4096.int 2 .5 3 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <stdio.h>
#include <fstream>
#include <vector> 
#include <deque> 
#include <string> 
#include <iostream>
#include "Point.h" 
#include "Bit.h"
#include "Interleaver.h"
#include <random> 
#include <chrono>
#include <math.h>


using namespace std;

// Function Declarations
Point returnClosestPoint(vector<Point> pointList, float xVal, float yVal); 
void  SOMAP(vector<Point> pointList, float xVal, float yVal, Point& current_point); 
int returnBitErrors(Point p1, Point p2);

int main(int argc, char *argv[]) {

	//parse the incoming constellation vector

	vector<Point> list;

	ifstream in_stream;

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	//Read in the file 
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	int n_point;
	int n_point_bits; 
	int n_dim;
	float point_x; 
	float point_y; 
	
	in_stream.open(argv[1]);

	in_stream >> n_point;
	in_stream >> n_dim;

	n_point_bits = (int)log2(n_point); 

	for (int i=0; i<n_point; i++) {
	    in_stream >> point_x;
	    in_stream >> point_y; 
	    Point temp_point(point_x, point_y); 
	    list.push_back(temp_point);
	    //cout << point_x << point_y << '\n'; 
	} 

	for (vector<Point>::iterator i = list.begin(); i != list.end(); ++i) {
		for (int j=0; j<n_point_bits; j++) {
			int n_code; 
		    in_stream >> n_code;
		    i->setCode(n_code);
		    Bit tempbit(n_code);
		    i->bits.push_back(tempbit);
		}
		//i->printCode(); 
	} 

	in_stream.close();

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	//Read in the interleaver 
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	int interleaver_size; 
	vector<int> interleaver = read_interleaver(argv[2], &interleaver_size);

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// Calculate parameters 
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	float start_power = atof(argv[3]);
	float d_power = atof(argv[4]);
	float end_power = atof(argv[5]); 
	
	float total_power = 0; 
	for (vector<Point>::iterator i = list.begin(); i != list.end(); ++i) {
		total_power = total_power + i->getPower(); 
	} 
	float avg_symbol_power = total_power/list.size(); 
	float avg_bit_power = avg_symbol_power/(float)sqrt(n_point); 

	//generate the power vector 
	float temp_power = start_power;
	float temp_factor; 
	vector<float> EbNo;
	vector<float> EsNo; 
	vector<float> factors; 
	while (temp_power <= end_power) {
		temp_factor = (temp_power*(float)sqrt(n_point))/10; 
		temp_factor = 2* (float)pow (10, temp_factor);
		temp_factor = sqrt(avg_symbol_power / (temp_factor)); 
		factors.push_back(temp_factor);
		EbNo.push_back(temp_power);
		EsNo.push_back(temp_power*(float)sqrt(n_point)); 
		temp_power += d_power; 
	} 


	cout << "Average Symbol power is: " << avg_symbol_power << "\n";
	cout << "Average bit power is: " << avg_bit_power << "\n";
	//cout << "Start power is: " << start_power << "\n";
	//cout << "End Power is: " << end_power << "\n"; 

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// Initialize randomness generators
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
    //Generate a random point 
   	//add noise
   	//print the random point 
   	// construct a trivial random generator engine from a time-based seed:
  	unsigned seedx = chrono::system_clock::now().time_since_epoch().count();
  	default_random_engine generator(seedx);

  	// generators for noise 
   	normal_distribution<float> noise (0.0,1.0);

	//generator for random number between 0 and 1
  	uniform_int_distribution<int> randombit(0,1); 

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// Perform the Algorithm 
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  	vector<int> bit_errors (10,0); 
  	vector<int> symbol_errors (10,0); 
  	vector<int> symbols (10,0); 
  	vector<int> bits (10,0); 
  	vector<int> deinterleaved_errors (10,0); 

  	vector<float>pS; 
  	vector<float>pB; 

  	for (int i=0; i<factors.size(); i++) {

  		cout << "You made it!" <<'\n'; 

  		int temp_bit_errors = 0; 
  		int temp_deinterleaved_bit_errors = 0; 
  		int temp_symbol_errors = 0; 
  		int temp_symbols = 0; 
  		int temp_bits = 0; 

  		float noise_point_x; 
  		float noise_point_y;
  		int addl_bit_errors;


  	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// Instead of choosing random points, choose bits, interleave them
	// code them into points, and then send the points, and de-interleave. 
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */	

  		deque<int> interleaved_bitstream; //does not matter ints or bits until you make them into points and add noise 
  		deque<Bit> out_bitstream; 
  		deque<Bit> deinterleaved_bitstream; 
  		int in_point; 
  		int out_point; 

  		int interleaved =0; 

	   	while (temp_deinterleaved_bit_errors < 100 || interleaved ==0) {

	   		if (interleaved_bitstream.size() < n_point_bits) {
	   			//generate a set of bits, will be taken from FEC later 
	   			deque<int> in_bitstream; 
	   			for (int i=interleaver_size; i>0; i--) {
					in_bitstream.push_back(randombit(generator)); 
				} 
				//cout << "you added " << in_bitstream.size() << " bits to your bitstream! \n"; 
				interleaved_bitstream = interleave_intstream(in_bitstream, interleaved_bitstream, interleaver, interleaver_size);

	   		}


	   		//translate in_bistream into a point. 
		   	in_point = 0; 
		   	vector<Bit> current_bits; 
		   	vector<int> current_code; 
		   	for ( int j=0; j<n_point_bits; j++) {
		   		//cout << "front of the bitstream is" <<in_bitstream.front() << '\n';
		   		int v = interleaved_bitstream.front();
		   		in_point = v + 2*in_point; 
		  		interleaved_bitstream.pop_front(); 
		   		//cout << "your point is: " << in_point << '\n'; 
		   		Bit myBit(v); 
		   		current_bits.push_back(myBit);
		   		current_code.push_back(v);
		   		//cout << v; 
		   	}
		   	//cout << '\n';

			Point current_point;
		   	for (vector<Point>::iterator j = list.begin(); j != list.end(); ++j) {
	   			int same = 1;
	   			for (int k = 0; k< current_code.size(); ++k) {
	   				if (current_code[k] != j->getCode()[k]) {
	   					same = 0; 
	   				}
	   			}
	   			if (same) {
	   				current_point = *j; 
	   				break;
	   			}
	   		}
	   		current_point.bits = current_bits; 
	   		//current_point.setAllCode(current_code);
	   		//cout << "code is: ";
	   		//current_point.printCode(); 

	    	//add some noise 
	   		noise_point_x = current_point.getX() + noise(generator)*factors[i]; 
	   		noise_point_y = current_point.getY() + noise(generator)*factors[i];
	   		SOMAP(list, noise_point_x, noise_point_y, current_point);
	   		Point guess_point = returnClosestPoint(list, noise_point_x, noise_point_y);
	   		//print the value 
	   		//cout << "Noisy point: " << noise_point_x << ' ' << noise_point_y << '\n'; 
	   		//cout << "Actual point: "<<current_point.getX() << ' ' << current_point.getY() << '\n';
	   		//cout << "Guess point: "<<guess_point.getX() << ' ' << guess_point.getY() << '\n';

	   		addl_bit_errors = returnBitErrors(current_point, guess_point); 
	   		if (addl_bit_errors > 0)
	   			temp_symbol_errors +=1; 

	   		temp_bit_errors += addl_bit_errors; 
	   		temp_symbols +=1; 
	   		temp_bits += sqrt(n_point); 

	   		//get the bits from the point 
	   		for (vector<Bit>::iterator j = current_point.bits.begin(); j!= current_point.bits.end(); ++j) {
	   			out_bitstream.push_back(*j); 
	   			//cout << " Calculating errors! value is: " << j->getValue() << " soft is: " << j->getSoft() << " hard is " << j->getHard() << '\n';

	   		}
	   		//cout << "size of out_bitstream is: " << out_bitstream.size() << '\n';

	   		//deinterleave the bit stream
	   		//clear the out_bitstream memory 
	   		if (out_bitstream.size() > interleaver_size) {
				deinterleaved_bitstream = deinterleave_bitstream(out_bitstream, deinterleaved_bitstream, interleaver, interleaver_size);
	   			out_bitstream.erase(out_bitstream.begin(), out_bitstream.begin() + interleaver_size); 
	   			interleaved =1; 
	   		}

	   		//Check for errors in the deinterleaved bitstream
	   		//clear the deinterleaved_bitstream memory 

	   		for (deque<Bit>::iterator it = deinterleaved_bitstream.begin(); it != deinterleaved_bitstream.end(); ++it) {
	   			if (it->getValue() != it->getHard()) 
	   				temp_deinterleaved_bit_errors++;
	   		} 

	   		deinterleaved_bitstream.clear(); 


	   	}
	   	cout << "EbNo: " << EbNo[i] << '\n'; 
	   	cout << "Factor is " << factors[i] << '\n';
	   	cout << "Total Bit Errors: "<< temp_bit_errors << '\n';
	   	cout << "Total Deinterleaved Bit Errors: " << temp_deinterleaved_bit_errors << '\n'; 
	   	cout << "Total Symbol Errors: "<< temp_symbol_errors << '\n';
	   	cout << "Total Bits: "<< temp_bits << '\n';
	   	cout << "Total Symbols: "<< temp_symbols<< '\n'; 
	   	cout << "Probability of Symbol Error: " << (float)temp_symbol_errors/(float)temp_symbols << '\n'; 

	   	pS.push_back((float)temp_symbol_errors / (float)temp_symbols );
	   	pB.push_back((float)temp_bit_errors / (float)temp_bits );

	   	bit_errors[i] = (temp_bit_errors);
	   	symbol_errors[i] = (temp_symbol_errors);
	   	symbols[i] = (temp_symbols);
	   	bits[i] = (temp_bits); 

	}

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// Print to a file 
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    ofstream MyFile ("output.csv");         //Opening file to print info to

    for (int i=0; i<pS.size(); i++) {
      MyFile << EsNo[i] << "," << pS[i] << "," << EbNo[i] << "," << pB[i] << ","  << endl;    
    }

     MyFile.close();

	return(0); 
}

//Subfunction: Given X,Y values and the list of points, update the soft-out values of the bits in the point. 

void SOMAP(vector<Point> pointList, float xVal, float yVal, Point& current_point) {
	//calculate all of the values? 
	//cout << current_point.bits.size() << '\n'; 
	deque<float> MO; 

	for (vector<Point>::iterator i = pointList.begin(); i != pointList.end(); ++i) {
		float distance = (xVal - i->getX())*(xVal - i->getX()) + (yVal - i->getY())*(yVal - i->getY());
		MO.push_back(distance);
		//cout << distance << "\n"; 
	}
	//find the min's given the marginalizations? 
	float min1 = 1000;
	float min0 = 1000; 
	for (int i = 0; i < current_point.bits.size(); i++) {
		min1 = 1000;
		min0 = 1000; 
		//find min of marginalizations 
		for (int j = 0; j != pointList.size(); ++j) {
			//cout << "distance is: " << MO[j] << '\n';
			//cout << "value is: " << pointList[j].getCode()[i] << '\n'; 
			if (pointList[j].getCode()[i] ==1) {
				if (MO[j] < min1)
					min1 = MO[j];
			}
			if (pointList[j].getCode()[i] ==0) {
				if (MO[j] < min0)
					min0 = MO[j];
			}
		}
		float somap = min1-min0;
		int hardout;
		if (somap>0)
			hardout = 0;
		else
			hardout = 1; 
		current_point.bits[i].setSoft(min1-min0);
		current_point.bits[i].setHard(hardout);

		//cout << "value is: " << current_point.bits[i].getValue() << " soft is: " << current_point.bits[i].getSoft() << " hard is " << current_point.bits[i].getHard() << '\n';
	}
	return; 
}; 


//Subfunction: Given a list of points and a XY power pair, 
//Find the closest point and return that point. 

Point returnClosestPoint(vector<Point> pointList, float xVal, float yVal) {
	float distance = 0; 
	float closestdistance = 100; 
	Point closestPoint(0,0); 
	for (vector<Point>::iterator i = pointList.begin(); i != pointList.end(); ++i) {
		distance = (xVal - i->getX())*(xVal - i->getX()) + (yVal - i->getY())*(yVal - i->getY());
		if (distance < closestdistance){
			closestdistance = distance;
			closestPoint.setX(i->getX()); 
			closestPoint.setY(i->getY()); 
			closestPoint.setPower(i->getPower()); 
			closestPoint.setAllCode(i->getCode()); 
		}
	}
	return closestPoint; 
}

//Subfunction: Given two points, return the number of bit errors between them 

int returnBitErrors(Point p1, Point p2){
	vector<int> code1 = p1.getCode();
	vector<int> code2 = p2.getCode(); 
	int nErrors = 0; 
	for (int i = 0; i<p1.getCode().size(); i++){
		if (code1.at(i) != code2.at(i))
			nErrors +=1; 
	}
	return nErrors; 
}
