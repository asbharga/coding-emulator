/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// To use this file type sra INTERLEAVER Q J K noiselevel
// ./sra PGI_8192.int 4 4 2048 .1 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <stdio.h>
#include <fstream>
#include <vector> 
#include <deque> 
#include <string> 
#include <iostream>
#include "Bit.h"
#include "Interleaver.h"
#include "accumulate.h"
#include <random> 
#include <chrono>
#include <math.h>

using namespace std; 

int main(int argc, char *argv[]) {

	char* IC_Name = argv[1];
	int Q=3;
	int J=3; 
	
	int IC_size; 
	vector<int> IC = read_interleaver(IC_Name, &IC_size);
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// do the encoding things 
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	deque<Bit> real_bits;
	deque<Bit> parity_bits; 

	Bit b1(1);
	b1.setSoft(-2);
	real_bits.push_back(b1);
	Bit b2(0);
	b2.setSoft(5);
	real_bits.push_back(b2);
	Bit b3(1);
	b3.setSoft(-1);
	real_bits.push_back(b3); 

	Bit p1(1);
	p1.setSoft(2);
	parity_bits.push_back(p1);
	Bit p2(1);
	p2.setSoft(-4);
	parity_bits.push_back(p2);
	Bit p3(1);
	p3.setSoft(6);
	parity_bits.push_back(p3); 

	for (int i = 0; i <3; i++)
		cout << real_bits[i].getValue() << " " <<real_bits[i].getSoft() << '\n'; 

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// do the decoding things 
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	vector<float> incoming_equality(9,0); 
	vector<float> incoming_interleave;
	vector<float> incoming_parity; 
	vector<float> incoming_deinterleave;

	for (int i = 0; i<4; i++) {
		incoming_interleave = equalitySiso(&real_bits,incoming_equality, Q);
		cout << "\n after " << i << " iterations the post-equality bits are "; 
		for (int i = 0; i <9; i++)
			cout << incoming_interleave[i] << " "; 

		incoming_parity = interleave_floatstream(incoming_interleave, IC, IC_size);
		cout << "\n after " << i << " iterations the interleaved bits are "; 
		for (int i = 0; i <9; i++)
			cout << incoming_parity[i] << " "; 
		
		incoming_deinterleave = paritySiso(parity_bits,incoming_parity, J);
		cout << "\n after " << i << " iterations the post-outer SISO bits are "; 
		for (int i = 0; i <9; i++)
			cout << incoming_deinterleave[i] << " "; 

		incoming_equality = deinterleave_floatstream(incoming_deinterleave, IC, IC_size);
		cout << "\n after " << i << " iterations the deinterleaved bits are "; 
		for (int i = 0; i <9; i++)
			cout << incoming_equality[i] << " "; 
	}
	return 0; 
}
