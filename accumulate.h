#include <chrono>
#include <random> 
#include <deque> 
#include <vector> 
#include "Bit.h"
#define sign(a) ( ( (a) < 0 )  ?  -1   : ( (a) > 0 ) )
deque<Bit> generate(int length);
deque<Bit> repeat(deque<Bit> bitstream, int length, int n);
deque<Bit> accumulate(deque<Bit> bitstream, int length);
deque<Bit> puncture(deque<Bit> bitstream, int length, int n);
deque<Bit> awgnNoise(deque<Bit> bitstream, float noisepower);

vector<float> equalitySiso(deque<Bit>* real_bits, vector<float> incoming, int Q);
vector<float> paritySiso(deque<Bit> parity_bits, vector<float> incoming, int J);
int errorcheck(deque<Bit> real_bits);

