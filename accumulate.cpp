#include "accumulate.h"

deque<Bit> generate(int length) {
	unsigned seedx = chrono::system_clock::now().time_since_epoch().count();
  	default_random_engine generator(seedx);

	//generator for random number between 0 and 1
  	uniform_int_distribution<int> randombit(0,1); 

 	deque<Bit> out_bitstream; 	
	for (int i=0; i<length; i++) {
		Bit temp_bit(randombit(generator));
		out_bitstream.push_back(temp_bit);
	}
	return out_bitstream; 
}

deque<Bit> repeat(deque<Bit> bitstream, int length, int n){
	deque<Bit> out_bitstream;
	for (int i=0; i<length; i++) {
		for (int j=0; j<n; j++)
			out_bitstream.push_back(bitstream[i]); 
	}
	return out_bitstream; 
}

deque<Bit> accumulate(deque<Bit> bitstream, int length) {
	deque<Bit> out_bitstream;
	int temp_value; 
	for (int i=0; i<length; i++) {
		if (i==0) {
			temp_value = bitstream[i].getValue();
		}
		else {
			temp_value = (bitstream[i].getValue() + out_bitstream[i-1].getValue())%2; 
		}
		Bit temp_bit(temp_value);
		out_bitstream.push_back(temp_bit);
	}
	return out_bitstream; 
}

deque<Bit> puncture(deque<Bit> bitstream, int length, int n) {
	deque<Bit> out_bitstream;
	for (int i=0; i<length; i++) {
		if ((i+1)%n == 0) {
			out_bitstream.push_back(bitstream[i]); 
		}
	}
	return out_bitstream; 
}

deque<Bit> awgnNoise(deque<Bit> bitstream, float noisepower) {
	deque<Bit> out_bitstream;
	// Initialize randomness generators
  	unsigned seedx = chrono::system_clock::now().time_since_epoch().count();
  	default_random_engine generator(seedx);
  	// generators for noise 
   	normal_distribution<float> noise (0.0,noisepower);
   	float soft; 
	for (int i=0; i<bitstream.size(); i++) {
		Bit temp_bit(bitstream[i].getValue());
		soft = (float)bitstream[i].getValue();
		if (soft==1)
			soft = -1;
		else 
			soft = 1; 
		soft = soft + noise(generator); 
		temp_bit.setSoft(soft); 
		out_bitstream.push_back(temp_bit);
	}

	return out_bitstream; 
}

vector<float> equalitySiso(deque<Bit>* real_bits, vector<float> incoming, int Q) {
	vector<float> outgoing(incoming.size(),0); 
	float totalmetric = 0; 
	//cout << "\nMIs in: ";
	for (int i = 0; i<(*real_bits).size(); i++) {
		totalmetric = (*real_bits)[i].getSoft();
		//cout << "incoming bit metric: " << totalmetric << '\n'; 
		for (int j = 0; j<Q; j++) {
			totalmetric = totalmetric + incoming[i*Q+j];
		}
		//cout << "adding all bit metrics: " << totalmetric << '\n'; 
		(*real_bits)[i].setMI(totalmetric - (*real_bits)[i].getSoft());
		//cout << (*real_bits)[i].getMI() << " " << "hard is: " << (*real_bits)[i].getHard() << " "; 
		for (int j = 0; j<Q; j++) {
			outgoing[i*Q+j] = totalmetric - incoming[i*Q +j]; 
			//cout << "previous metric: " << incoming[i*Q+j] << " new metric: " << outgoing[i*Q+j] << '\n';
		}
	}
	//cout << "Did Equality Siso Check \n"; 
	return outgoing; 

}

vector<float> paritySiso(deque<Bit> parity_bits, vector<float> Uj, int J) {
	int ICsize = Uj.size();
	vector<float> Vj(ICsize,0); 
	vector<float> forwards(ICsize +1,0); 
	vector<float> backwards(ICsize +1,0); 
	vector<float> outgoing(ICsize,0); 

	//cout << "\nVj: ";
	for (int i=0; i<ICsize; i++) {
		if ((i+1)%J ==0) {
			Vj[i] = parity_bits[(i+1-J)/J].getSoft(); 
		}
		//cout << Vj[i] << " ";  
	}
	
	//forwards 
	//cout << "\nForwards: ";
	forwards[0] = 10000000; 
	//cout << forwards[0] << " "; 
	for (int i=1; i<ICsize; i++) {
		forwards[i] = min(abs(forwards[i-1]),abs(Uj[i-1]))*sign(forwards[i-1])*sign(Uj[i-1]) + Vj[i-1]; 
		//cout << forwards[i] << " "; 
	}
	//cout << "Completed Forwards \n"; 

	//backwards
	//cout << "\nBackwards: ";
	float last; 
	backwards[ICsize] = 10000000;  
	for (int i=ICsize-1; i>0; i--) {
		last = backwards[i+1] + Vj[i]; 
		backwards[i] = min(abs(last),abs(Uj[i]))*sign(last)*sign(Uj[i]);
	}
	//for (int i=0; i<backwards.size(); i++)
		//cout << backwards[i] << " ";

	//cout << "\nOutgoing: ";
	for (int i=0; i<ICsize; i++) {
		float bw = backwards[i+1] + Vj[i]; 
		outgoing[i] = min(abs(forwards[i]),abs(bw))*sign(forwards[i])*sign( bw); 
		if (abs(outgoing[i]) ==0)
			outgoing[i] = 0; 
		//cout << outgoing[i] << " ";  
	}

	//cout << "Did Parity Siso Check \n";
	return outgoing; 
}

int errorcheck(deque<Bit> real_bits) {
	int nerrors = 0; 
	for (deque<Bit>::iterator i = real_bits.begin(); i!=real_bits.end(); ++i) {
		if (i->getHard() != i->getValue())
			nerrors++; 
	}
	return nerrors; 
}