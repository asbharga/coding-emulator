#include "Point.h"


Point::Point(float x, float y): xLoc(x), yLoc(y) {
	power =  x*x + y*y; 
	//code = 0; 
}

Point::Point() {}


Point::~Point() {} 

float Point::getX() {
	return xLoc;
}

float Point::getY() {
	return yLoc;
}

float Point::getPower() {
	return power;
}

vector<int> Point::getCode() {
	return code; 
}

void Point::setPower(float p) {
	power = p; 
}

void Point::setX(float x){
	xLoc = x;
}

void Point::setY(float y){
	yLoc = y;
}

void Point::setCode(int c) {
	this->code.push_back(c); 
}

void Point::setAllCode(vector<int> c) {
	code = c; 
}

void Point::printCode() {
	for (vector<int>::iterator i = code.begin(); i != code.end(); ++i) {
		cout << *i;
	}
	cout << '\n'; 
}

Point& Point::operator=(Point &rhs) {
    // Check for self-assignment!
    if (this == &rhs)      // Same object?
      return *this;        // Yes, so skip assignment, and just return *this.

    this->setX(rhs.getX());
    this->setY(rhs.getY());
    this->setPower(rhs.getPower());
    this->setAllCode(rhs.getCode());

    return *this;
  }

