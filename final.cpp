/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// To use this file type sra INTERLEAVER Q J K start_EBNO, d_EBNO, end_EBNO, CONSTELLATION CHANNEL_INTERLEAVER
// ./final PGI_8192.int 4 4 2048 2 1 3 BiAWGN.txt PGI_4096.int
//./final PGI_8192.int 4 4 2048 1.9 .1 2.1 QAM16.txt PGI_4096.int
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <stdio.h>
#include <fstream>
#include <vector> 
#include <deque> 
#include <string> 
#include <iostream>
#include "Bit.h"
#include "Interleaver.h"
#include "accumulate.h"
#include <random> 
#include <chrono>
#include <math.h>

using namespace std; 

int main(int argc, char *argv[]) {

	char* IC_Name = argv[1];
	int Q = atoi(argv[2]);
	int J = atoi(argv[3]);
	int K = atoi(argv[4]);
	
	int IC_size; 
	vector<int> IC = read_interleaver(IC_Name, &IC_size);

	float start_power = atof(argv[5]);
	float d_power = atof(argv[6]);
	float end_power = atof(argv[7]); 

	int n_point;
	int n_point_bits; 
	vector<Point> list = generatePointList( argv[8], &n_point, &n_point_bits); 
	cout << "\nread in the list"; 

	int channel_IC_size; 
	vector<int> channel_IC = read_interleaver(argv[9], &channel_IC_size);
	
	float total_power = 0; 
	for (vector<Point>::iterator i = list.begin(); i != list.end(); ++i) {
		total_power = total_power + i->getPower(); 
	} 
	float avg_symbol_power = total_power/list.size(); 
	float avg_bit_power = avg_symbol_power/(float)sqrt(n_point); 

	//generate the power vector 
	float temp_power = start_power;
	float temp_factor; 
	vector<float> EbNo;
	vector<float> EsNo; 
	vector<float> factors; 
	while (temp_power <= end_power) {
		if (n_point_bits ==1) 
			temp_factor = 1/( pow (10,temp_power/10) ); 
		else 
			temp_factor = 2.5/( sqrt(2) * pow (10,temp_power/10) ); 
		factors.push_back(temp_factor);
		EbNo.push_back(temp_power);
		EsNo.push_back(temp_power*(float)sqrt(n_point)); 
		temp_power += d_power; 
	} 

	cout << "\nNumber of factors is: " << factors.size(); 
	cout << "\n"<< factors[0] << " " << factors[1] << " " << factors[2]; 



	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// Print to a file (setup)
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	ofstream MyFile ("outputFinal_goodEbNoQAM.csv");         //Opening file to print info to
	MyFile << "EbNo, Nbits, NBlocks, PBerr[0], PBerr[1], PBerr[2], PBerr[3], PBerr[4], PBerr[5], PBerr[6], PBerr[7], PBerr[8], PBerr[9], PBerr[10],";   
	MyFile << "PBerr[11], PBerr[12], PBerr[13], PBerr[14], PBerr[15], PBerr[16], PBerr[17], PBerr[18], PBerr[19]" << endl;    
	    
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// run the sim 
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	for (int E = 0; E < factors.size(); E++) {
		vector<float> codeword_errors(20,0);
		vector<float> bit_errors(20,0); 
		int n_codewords = 0; 
		int n_bits = 0; 
		int temp_errors; 
		cout << "\nYou're on factor number " << E << " out of " << factors.size(); 


		while (codeword_errors[19] < 40) {

			deque<Bit> real_bits;
			deque<Bit> w_bits; 
			deque<Bit> u_bits; 
			deque<Bit> v_bits; 
			deque<Bit> parity_bits; 
			deque<Bit> channel_bits; 
			deque<Bit> channel_int_bits; 
			deque<Bit> channel_deint_bits; 
			deque<Bit> real_bits_out; 
			deque<Bit> parity_bits_out; 

			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
			// do the encoding things 
			* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

			real_bits = generate(K);
			//cout << "number of real bits is: " << real_bits.size() << '\n';
			//for (int i = 0; i <3; i++)
			//	cout << real_bits[i].getValue() << '\n'; 
			w_bits = repeat(real_bits, K, Q);
			//cout << "number of repeat bits is: " << w_bits.size() << '\n';
			//for (int i = 0; i <9; i++)
			//	cout << w_bits[i].getValue() << '\n'; 
			u_bits = interleave_bitstream(w_bits, u_bits, IC, IC_size);
			//cout << "number of interleave bits is: " << u_bits.size() << '\n';
			//for (int i = 0; i <9; i++)
			//	cout << u_bits[i].getValue() << '\n'; 
			v_bits = accumulate(u_bits, u_bits.size());
			//cout << "number of accumulate bits is: " << v_bits.size() << '\n';
			//for (int i = 0; i <9; i++)
			//	cout << v_bits[i].getValue() << '\n'; 
			parity_bits = puncture(v_bits, v_bits.size(), J); 
			//cout << "number of parity bits is: " << parity_bits.size() << '\n';
			//for (int i = 0; i <3; i++)
			//	cout << parity_bits[i].getValue() << '\n'; 
			

			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
			// go through the channel 
			* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

			if (n_point_bits ==1) { //generate half of the noise variables if you're Bi-AWGN
				//cout << "\nBi-AWGN mode!";
				real_bits_out = awgnNoise(real_bits, factors[E]);
				parity_bits_out = awgnNoise(parity_bits,factors[E]);
			}
			else { //do all of the stuff 
				//real stuff 
				//cout << "\nQAM mode!";
				channel_bits = real_bits; 
				for (int i = 0; i<parity_bits.size(); i++)
					channel_bits.push_back(parity_bits[i]);

				channel_int_bits = interleave_bitstream(channel_bits, channel_int_bits, channel_IC, channel_IC_size);
				channel_int_bits = QAM16Noise(channel_int_bits, factors[E], list, n_point_bits, n_point); 
				channel_deint_bits = deinterleave_bitstream(channel_int_bits, channel_deint_bits, channel_IC, channel_IC_size);

				for (int i = 0; i<K; i++) {
					real_bits_out.push_back(channel_deint_bits[0]);
					channel_deint_bits.pop_front(); 
				}
				parity_bits_out = channel_deint_bits; 
			}

			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
			// do the decoding things 
			* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
			vector<float> incoming_equality(IC_size,0); 
			vector<float> incoming_interleave;
			vector<float> incoming_parity; 
			vector<float> incoming_deinterleave;

			for (int i = 0; i<20; i++) {
				incoming_interleave = equalitySiso(&real_bits_out,incoming_equality, Q);
				temp_errors = errorcheck(real_bits_out);
				//cout << "\nNumber of errors for iteration " << i << " is :" << temp_errors; 
				bit_errors[i] = bit_errors[i] + temp_errors; 
				if (temp_errors>1) {
					codeword_errors[i] = codeword_errors[i] +1; 
					if (i==19)
						cout << "\nnumber of codeword errors is: " << codeword_errors[19];
				}

				if (bit_errors[i] == 0)
					break; 

				incoming_parity = interleave_floatstream(incoming_interleave, IC, IC_size);	
				incoming_deinterleave = paritySiso(parity_bits_out,incoming_parity, J);
				incoming_equality = deinterleave_floatstream(incoming_deinterleave, IC, IC_size);
			}
			n_codewords = n_codewords+1;
			n_bits = n_bits + K; 

			if (n_codewords%200 ==0)
				cout << "\nnumber of iterations is: " << n_codewords; 
			if (n_codewords%20 ==0)
				cout << "."; 
		}

		cout << "\nEbNo is: " << EbNo[E]; 
		for (int i = 0; i<20; i++) {
			//cout << "\nnumber of bit errors for " << i << " iterations: " << bit_errors[i];
			//cout << "\nnumber of block errors for " << i << " iterations: " << codeword_errors[i];
			cout << "\nPercent of bit errors for " << i << " iterations: " << (float)bit_errors[i]/n_bits;
			cout << "\nPercent of block errors for " << i << " iterations: " << (float)codeword_errors[i]/n_codewords;
		}
		cout << "\n\n\n"; 

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		// Print to a file (results)
		* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
		MyFile << EbNo[E] << ',' << n_bits << ',' << n_codewords << ','; 
		for (int i=0; i<20; i++)
			MyFile << (float)(codeword_errors[i]/n_codewords) << ',';
		MyFile << endl; 
	}
	return 0; 
}