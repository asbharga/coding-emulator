/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// To use this file type sra INTERLEAVER Q J K noiselevel
// ./sra PGI_8192.int 4 4 2048 .1 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <stdio.h>
#include <fstream>
#include <vector> 
#include <deque> 
#include <string> 
#include <iostream>
#include "Bit.h"
#include "Interleaver.h"
#include "accumulate.h"
#include <random> 
#include <chrono>
#include <math.h>

using namespace std; 

int main(int argc, char *argv[]) {

	char* IC_Name = argv[1];
	int Q = atoi(argv[2]);
	int J = atoi(argv[3]);
	int K = atoi(argv[4]);
	float N = atof(argv[5]);
	
	int IC_size; 
	vector<int> IC = read_interleaver(IC_Name, &IC_size);
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// do the encoding things 
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	deque<Bit> real_bits;
	deque<Bit> w_bits; 
	deque<Bit> u_bits; 
	deque<Bit> v_bits; 
	deque<Bit> parity_bits; 


	real_bits = generate(K);
	cout << "number of real bits is: " << real_bits.size() << '\n';
	for (int i = 0; i <3; i++)
		cout << real_bits[i].getValue() << '\n'; 
	w_bits = repeat(real_bits, K, Q);
	cout << "number of repeat bits is: " << w_bits.size() << '\n';
	for (int i = 0; i <9; i++)
		cout << w_bits[i].getValue() << '\n'; 
	u_bits = interleave_bitstream(w_bits, u_bits, IC, IC_size);
	cout << "number of interleave bits is: " << u_bits.size() << '\n';
	for (int i = 0; i <9; i++)
		cout << u_bits[i].getValue() << '\n'; 
	v_bits = accumulate(u_bits, u_bits.size());
	cout << "number of accumulate bits is: " << v_bits.size() << '\n';
	for (int i = 0; i <9; i++)
		cout << v_bits[i].getValue() << '\n'; 
	parity_bits = puncture(v_bits, v_bits.size(), J); 
	cout << "number of parity bits is: " << parity_bits.size() << '\n';
	for (int i = 0; i <3; i++)
		cout << parity_bits[i].getValue() << '\n'; 
	real_bits = awgnNoise(real_bits, N);
	parity_bits = awgnNoise(parity_bits,N);

	for (int i = 0; i <5; i++)
		cout << real_bits[i].getValue() << " " <<real_bits[i].getSoft() << '\n'; 

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// do the decoding things 
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	vector<float> incoming_equality(IC_size,0); 
	vector<float> incoming_interleave;
	vector<float> incoming_parity; 
	vector<float> incoming_deinterleave;

	for (int i = 0; i<20; i++) {
		incoming_interleave = equalitySiso(&real_bits,incoming_equality, Q);

		cout << "after " << i << " iterations the number of errors is: " << errorcheck(real_bits) << '\n'; 

		incoming_parity = interleave_floatstream(incoming_interleave, IC, IC_size);
		
		incoming_deinterleave = paritySiso(parity_bits,incoming_parity, J);

		incoming_equality = deinterleave_floatstream(incoming_deinterleave, IC, IC_size);
	}
	return 0; 
}
