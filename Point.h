#ifndef POINT_H
#define POINT_H

#include "math.h"
#include "string.h"
#include <vector> 
#include <stdio.h>
#include <iostream>
#include "Bit.h"

using namespace std;

class Point{
private:
	float xLoc;
	float yLoc;
	float power; 
	vector<int> code; 



public:
	Point(float xLoc, float yLoc);
	Point();
	~Point(); 
	float getX();
	float getY();
	float getPower(); 
	vector<int> getCode(); 
	void setX(float x);
	void setY(float y);
	void setPower(float p);
	void setCode(int c); 
	void setAllCode(vector<int> c);
	void printCode(); 
	Point& operator=(Point &rhs);
	vector<Bit> bits; 

};


#endif