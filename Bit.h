#ifndef BIT_H
#define BIT_H

#include "math.h"
#include "string.h"
#include <stdio.h>
#include <iostream>

using namespace std;

class Bit{
private:
	int value; 
	float soft;
	int hard; 
	float MI; 


public:
	Bit(int _value): value(_value) {};
	Bit() {};
	~Bit() {}; 
	int getValue() { return value;};
	float getSoft(){ return soft;};
	int getHard(){ return hard;}; 
	float getMI(){ return MI;}
	void setValue(int v) {value = v;};
	void setSoft(float s){ soft = s;};
	void setHard(int h){ hard = h;}; 
	void setMI(int m) { 
		MI= m; 
		if ((MI + soft) > 0) 
			hard = 0;
		else 
			hard = 1;
		}
};


#endif